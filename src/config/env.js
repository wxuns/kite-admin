// 配置编译环境和线上环境之间的切换

let baseUrl = ''
let iconfontVersion = ['567566_82imxaft0by', '2874538_ap2nbwldpsr']
let iconfontUrl = `//at.alicdn.com/t/font_$key.css`
const env = process.env
if (env.NODE_ENV == 'development') {
    baseUrl = `//k.me/k-avue` // 开发环境地址
} else if (env.NODE_ENV == 'production') {
    baseUrl = `` //生产环境地址
} else if (env.NODE_ENV == 'test') {
    baseUrl = `` //测试环境地址
}
let codeUrl = `${baseUrl}/captcha`
export {
    baseUrl,
    iconfontUrl,
    iconfontVersion,
    codeUrl,
    env
}