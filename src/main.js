import Vue from 'vue';
import axios from './router/axios';
import VueAxios from 'vue-axios';
import App from './App';
import router from './router/router';
import './permission'; // 权限
import './error'; // 日志
import './cache';//页面缓冲
import store from './store';
import { loadStyle } from './util/util'
import * as urls from '@/config/env';
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import AVUE from '@smallwei/avue'
import '@smallwei/avue/lib/index.css'
import { iconfontUrl, iconfontVersion } from '@/config/env';
import i18n from './lang' // Internationalization
import './styles/common.scss';
import basicBlock from './components/basic-block/main'
import basicContainer from './components/basic-container/main'
import crudCommon from '@/mixins/crud.js'
import dayjs from 'dayjs'
import website from '@/config/website'
import Editor from 'bin-ace-editor'
// 按需引入需要的语言包皮肤等资源
require('brace/ext/emmet') // 如果是lang=html时需引入
require('brace/ext/language_tools') // language extension

require('brace/mode/json')
require('brace/snippets/json')
require('brace/mode/mysql')
require('brace/snippets/mysql')
require('brace/mode/javascript')
require('brace/snippets/javascript')
require('brace/mode/stylus')
require('brace/snippets/stylus')
require('brace/theme/chrome')
require('brace/theme/terminal')
require('brace/theme/textmate')
// 注册组件后即可使用
Vue.component(Editor.name, Editor)
window.$crudCommon = crudCommon
Vue.prototype.$dayjs = dayjs
Vue.prototype.website = website;
Vue.config.productionTip = false;
Vue.use(VueAxios, axios)
Vue.use(Element, {
  i18n: (key, value) => i18n.t(key, value)
})
Vue.use(AVUE, {
  i18n: (key, value) => i18n.t(key, value)
})
//注册全局容器
Vue.component('basicContainer', basicContainer)
Vue.component('basicBlock', basicBlock)
// 加载相关url地址
Object.keys(urls).forEach(key => {
  Vue.prototype[key] = urls[key];
})
// 动态加载阿里云字体库
iconfontVersion.forEach(ele => {
  loadStyle(iconfontUrl.replace('$key', ele));
})
new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')